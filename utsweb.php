<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@500&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>UTS PBW</title>
    <style>
        *{
            font-family: 'Poppins', sans-serif;
        }

        .nav {
            background: rgb(60, 100, 187);
        }

        nav a {
            color: white;
        }

        .btn-submit, .btn-submit:hover{
            padding: 12px 70px;
            background-color: rgb(60, 100, 187);
            color: white;
        }

        .site-footer
        {
        background-color: rgb(60, 100, 187);
        padding:45px 0 20px;
        font-size:15px;
        line-height:24px;
        color:white;
        }
        .site-footer hr
        {
        border-top-color:#bbb;
        opacity:0.5
        }
        .site-footer hr.small
        {
        margin:20px 0
        }
        .site-footer h6
        {
        color: #F6EE01;
        font-size:16px;
        text-transform:uppercase;
        margin-top:5px;
        letter-spacing:2px
        }
        .site-footer a
        {
        color:white;
        }
        .site-footer a:hover
        {
        color:#3366cc;
        text-decoration:none;
        }
        .footer-links
        {
        padding-left:0;
        list-style:none
        }
        .footer-links li
        {
        display:block
        }
        .footer-links a
        {
        color:white
        }
        .footer-links a:active,.footer-links a:focus,.footer-links a:hover
        {
        color:#3366cc;
        text-decoration:none;
        }
        .footer-links.inline li
        {
        display:inline-block
        }
        .site-footer .social-icons
        {
        text-align:right
        }
        .site-footer .social-icons a
        {
        width:40px;
        height:40px;
        line-height:40px;
        margin-left:6px;
        margin-right:0;
        border-radius:100%;
        background-color:#33353d
        }
        .copyright-text
        {
        margin:0
        }
        @media (max-width:991px)
        {
        .site-footer [class^=col-]
        {
            margin-bottom:30px
        }
        }
        @media (max-width:767px)
        {
        .site-footer
        {
            padding-bottom:0
        }
        .site-footer .copyright-text,.site-footer .social-icons
        {
            text-align:center
        }
        }
        .social-icons
        {
        padding-left:0;
        margin-bottom:0;
        list-style:none
        }
        .social-icons li
        {
        display:inline-block;
        margin-bottom:4px
        }
        .social-icons li.title
        {
        margin-right:15px;
        text-transform:uppercase;
        color:#96a2b2;
        font-weight:700;
        font-size:13px
        }
        .social-icons a{
        background-color:#eceeef;
        color:#818a91;
        font-size:16px;
        display:inline-block;
        line-height:44px;
        width:44px;
        height:44px;
        text-align:center;
        margin-right:8px;
        border-radius:100%;
        -webkit-transition:all .2s linear;
        -o-transition:all .2s linear;
        transition:all .2s linear
        }
        .social-icons a:active,.social-icons a:focus,.social-icons a:hover
        {
        color:#fff;
        background-color:#29aafe
        }
        .social-icons.size-sm a
        {
        line-height:34px;
        height:34px;
        width:34px;
        font-size:14px
        }
        .social-icons a.facebook:hover
        {
        background-color:#3b5998
        }
        .social-icons a.twitter:hover
        {
        background-color:#00aced
        }
        .social-icons a.linkedin:hover
        {
        background-color:#007bb6
        }
        .social-icons a.dribbble:hover
        {
        background-color:#ea4c89
        }
        @media (max-width:767px)
        {
        .social-icons li.title
        {
            display:block;
            margin-right:0;
            font-weight:600
        }
        }
    </style>
  </head>
  
  <body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-ligt nav p-3">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Pemrograman Berbasis WEB</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>
    </header> 

      <div class="container">
        <div class="card mt-4 rounded-3 shadow">
            <div class="card-body">
                <h5 class="mt-2 text-center"><strong>Form Registrasi Mahasiswa</strong></h5>     
                                    
                <form method="POST" action="utsweb.php">
                        <div class="row mt-4 justify-content-center">
                            <div class="col-lg-8 mb-3">
                                <label for="exampleInputEmail1" class="form-label">Nama</label>
                                <input type="text" class="form-control shadow-sm" name="name" placeholder="Masukan Nama" aria-describedby="emailHelp">
                            </div>

                            <div class="col-lg-8 mb-3">
                                <label for="exampleInputEmail1" class="form-label">NPM</label>
                                <input type="number" class="form-control shadow-sm" name="npm" placeholder="Masukan NPM" aria-describedby="emailHelp">
                            </div>
                            
                            <div class="col-lg-8 mb-3">
                                <label for="floatingTextarea2">Kelas</label>
                                <select class="form-select" name="kelas" aria-label="Default select example">
                                    <option selected>Pilih Kelas</option>
                                    <option>4KA01</option>
                                    <option>4KA02</option>
                                    <option>4KA03</option>
                                    <option>4KA04</option>
                                    <option>4KA05</option>
                                    <option>4KA06</option>
                                    <option>4KA07</option>
                                    <option>4KA08</option>
                                    <option>4KA09</option>
                                    <option>4KA10</option>
                                    <option>4KA11</option>
                                    <option>4KA12</option>
                                    <option>4KA13</option>
                                    <option>4KA14</option>
                                </select>
                            </div>

                            <div class="col-lg-8 mb-3">
                                <label for="floatingTextarea2">Alamat</label>
                                <textarea class="form-control shadow-sm" name="alamat" placeholder="Masukan Alamat" id="floatingTextarea2" style="height: 100px"></textarea>
                            </div>

                            <div class="col-lg-12 text-center mt-4">
                                <button type="submit" name="submit" value="submit" class="btn btn-submit">Simpan</button>
                            </div>
                        </div>
                </form>
            </div>
        </div>

        <div class="card mt-4 mb-4 rounded-3 shadow">
            <div class="card-body">
                <h5 class="mt-2 text-center"><strong>Table Data Mahasiswa</strong></h5>     
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0" border="1" >
                        <thead class="text-center">
                            <tr>
                                <th>Nama</th>
                                <th>NPM</th>
                                <th>Kelas</th>
                                <th>Alamat</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td><?php echo $_POST['name'];?></td>
                                <td><?php echo $_POST['npm'];?></td>
                                <td><?php echo $_POST['kelas'];?></td>
                                <td><?php echo $_POST['alamat'];?></td>
                            </tr>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>

      <!-- Site footer -->
    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>About</h6>
            <p class="text-justify">This website was created using HTML 5, CSS 3, and the PHP programming language, assisted by Bootstrap as a CSS framework</p>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Categories</h6>
            <ul class="footer-links">
              <li><a href="http://scanfcode.com/category/c-language/">C</a></li>
              <li><a href="http://scanfcode.com/category/front-end-development/">UI Design</a></li>
              <li><a href="http://scanfcode.com/category/back-end-development/">PHP</a></li>
              <li><a href="http://scanfcode.com/category/java-programming-language/">Java</a></li>
              <li><a href="http://scanfcode.com/category/android/">Android</a></li>
              <li><a href="http://scanfcode.com/category/templates/">Templates</a></li>
            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Quick Links</h6>
            <ul class="footer-links">
              <li><a href="http://scanfcode.com/about/">About Us</a></li>
              <li><a href="http://scanfcode.com/contact/">Contact Us</a></li>
              <li><a href="http://scanfcode.com/contribute-at-scanfcode/">Contribute</a></li>
              <li><a href="http://scanfcode.com/privacy-policy/">Privacy Policy</a></li>
              <li><a href="http://scanfcode.com/sitemap/">Sitemap</a></li>
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Build by Rizki Awalia Fatoni (16118284).
            </p>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>
              <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>   
            </ul>
          </div>
        </div>
      </div>
</footer>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>